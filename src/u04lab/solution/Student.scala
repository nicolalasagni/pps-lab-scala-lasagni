package u04lab.solution

trait Student {
  def name: String
  def year: Int
  def enrolling(courses: Course*): Unit // the student participates to a Course
  def courses: Set[String] // names of course the student participates to
  def hasTeacher(teacher: String): Boolean // is the student participating to a course of this teacher?
}

trait Course {
  def name: String
  def teacher: String
}

object Student {

  def apply(name: String, year: Int = 2017):Student = StudentImpl(name, year)

  case class StudentImpl(override val name: String,
                         override val year: Int,
                         private var studentCourses: Set[Course] = Set.empty) extends Student {

    override def enrolling(courses: Course*): Unit = {
      courses foreach(course => studentCourses = studentCourses + course)
    }

    override def courses: Set[String] = { studentCourses map( course => course.name) }

    override def hasTeacher(teacher: String): Boolean = { studentCourses map(course => course.teacher) contains teacher }

  }

}

object Course {

  def apply(name: String, teacher: String):Course = CourseImpl(name, teacher)

  case class CourseImpl(override val name: String, override val teacher: String) extends Course

}

object Try extends App {

  val cPPS = Course("PPS","Viroli")
  val cPCD = Course("PCD","Ricci")
  val cSDR = Course("SDR","D'Angelo")

  val s1 = Student("mario",2015)
  s1.enrolling(cPPS, cSDR)

  val s2 = Student("gino",2016)
  s2.enrolling(cPPS)

  val s3 = Student("rino") //defaults to 2017
  s3.enrolling(cPPS)
  s3.enrolling(cPCD)
  s3.enrolling(cSDR)

  println(s1.courses, s2.courses, s3.courses) // (Set(PPS, PCD),Set(PPS),Set(PPS, PCD, SDR))
  println(s1.hasTeacher("Ricci")) // false
}

/** Hints:
  * - simply implement Course, e.g. with a case class
  * - implement Student with a StudentImpl keeping a private Set of courses
  * - try to implement in StudentImpl method courses with map
  * - try to implement in StudentImpl method hasTeacher with map and find
  * - check that the two println above work correctly
  * - refactor the code so that method enrolling accepts a variable argument Course*
  */
